# Genome databases (Tripal pages)

## Access

A Tripal database is available for **each species** of the Phaeoexplorer project, 
containing all strains of the same species.

A link to the genome database (GGA / Tripal) is available at the top of the [organism page](./portal.md#organism-pages) 
on the website:
 
![gga logo](./imgs/gga_logo_website.png)
 
## Tripal Home page

The Tripal home page summarises all available organisms and analyses for each species.

On this page you can see the number of sequences/features and some basic information about 
the analyses and the organisms.

![tripal home page](./imgs/tripal_homepage.png)

## Search 

### Search for a feature

The features (mRNAs and polypeptides) can be:

- searched via the search box, by its ID or one of its properties (e.g. Ec32 ortholog description)
- accessed by clicking on the `Features` dropdown menu under the organism's name (leftmost of the top menu)

![tripal features](./imgs/tripal_features.png)

### Advanced Search Tips

#### Wildcard search with `*`

Examples:

- `*transferase` (matches: `acyltransferase`, `sulfotransferase`, `methyltransferase`, ...)
- `*F_contig199.*` (matches all features on contig199 of the female genome)

#### Fuzzy search 

When you don't know how to exactly spell the keywords, you can use fuzzy search. 
Fuzzy search allows you to search for similar words. You use the `~` character at the end of your keyword for 
fuzzy search (`keyword~`). 

Examples:

- `transferaase~` (matches: `transferase`)
- `acltransferase~` (matches: `acyltransferase`)

#### Boolean operators

`+` and `-`: `+` means must be present; `-` means must not be present 

Example:

`+transferase -acyl` (includes the word `transferase`, excludes the word `̀acyl`)

`AND`, `OR`, `NOT` operator and combination search

Example:

`transferase AND (acyl OR glycosyl) NOT "family 31"`

## mRNA pages

On each mRNA page, a left menu is displayed with the following sections: 

- **Overview**: name and ID
- **Alignments**: gives direct access to the mRNA in the genome browser
- **Analyses**: list of analyses linked to this mRNA
- **Properties**: list of properties
- **Relationships**: links to related features (polypeptide, CDS, UTR, ...)
- **Sequences**: where you can get the nucleic acid and protein sequences

![tripal feature sequences](./imgs/tripal_feature_sequences.png)
# Accessing data

The project data can be retrieved:

- from the [Rhodoexplorer web portal](https://rhodoexplorer.sb-roscoff.fr) on the `Download`
 page or on the organism pages (see the [web portal](./portal.md) page)

- from the ABiMS server, on `/shared/projects/rhodoexplorer` (see [section](#from-the-abims-server) below)
  
## From the ABiMS server

### Create an account on the ABiMS platform

See section "Request an account" in the [Contact us](./support.md) page.

### Data location

The data is located on `/shared/projects/rhodoexplorer`

### Access the data via SFTP (file exchange)

Download files on your computer.

![data access sftp](./imgs/data_access_sftp.png)

### Access the data via SSH (interactive terminal)

Directly access the ABiMS computing cluster:

```
ssh <my-login>@bioinfo.sb-roscoff.fr
newgrp rhodoexplorer
cd /shared/projects/rhodoexplorer
```

In order to be able to run your analyses on the ABiMS cluster, you need **to ask for a project workspace** at the [ABiMS Cluster Account Manager](https://my.sb-roscoff.fr/manager2).





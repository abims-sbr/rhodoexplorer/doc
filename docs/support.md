# Contact us

## Support

If you encounter any problems with the Rhodoexplorer resources or with accessing the data, 
contact [support.abims@sb-roscoff.fr](mailto:support.abims@sb-roscoff.fr).

## Request an account

If you need an account on the ABiMS platform with access to the restricted parts of the website and the data, 
please fill out the form at the [ABiMS Cluster Account Manager](https://my.sb-roscoff.fr/manager2/register), 
explaining that you request an access to the restricted-access Rhodoexplorer resources.

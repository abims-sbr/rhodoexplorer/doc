# Demos

These demos are based on the very similar [Phaoexplorer web portal](https://rhodoexplorer.sb-roscoff.fr).

## Web portal

<video width="572" controls><source src="../demos/demo_phaeoexplorer_portal.mp4" type="video/mp4">
Your browser does not support the video tag.</video>

## Genome browser and database

<video width="572" controls><source src="../demos/demo_tripal_jbrowse_gga.mp4" type="video/mp4">
Your browser does not support the video tag.</video>

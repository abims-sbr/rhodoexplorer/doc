# Genome browsers (JBrowse)

## Access

A JBrowse genome browser is available for **each species** of the Phaeoexplorer project, 
containing all strains of the same species.

A link to the genome browser is available at the top of the [organism page](./portal.md#organism-pages)  
on the website: 
  
![jbrowse logo](./imgs/jbrowse_logo_website.png)

## Display tracks

By default, a predefined track will be displayed when you access the genome browser

![jbrowse](./imgs/jbrowse.png)

You can tick other tracks to display them.

## Browse the genome

You can browse the genome by:

- moving left and right with your mouse or with the top arrows
- zooming in and out
- switching contig/chromosome with the dropdown menu

![jbrowse_navigate.png](./imgs/jbrowse_navigate.png)

## Switch to another strain/sex

To select another strain/sex, click on `Genome` in the menu. There you can select whichever strain/sex you want
and the window will refresh to display it.

![jbrowse select strain](./imgs/jbrowse_select_strain.png)

## mRNA

Left-click on a feature to display related data.

![jbrowse feature data](./imgs/jbrowse_feature_data.png)

Right-click on a feature and choose `View transcript report` to switch to the Tripal page of the mRNA.

![jbrowse transcript report link](./imgs/jbrowse_transcript_report_link.png)

# BLAST servers

## General

You can access the BLAST server from the dropdown top menu `BLAST`. 

On the BLAST server, you can query the genomes, the predicted transcripts, the proteins,
    and the de-novo transcriptomes of the Rhodoexplorer project plus additional relevant species

## Using the BLAST interface

### BLAST query

1. Paste query sequence(s) or drag file containing query sequence(s) in FASTA format in the text field
2. Choose one or more databases
3. Optional: set advanced parameters (see help button)
4. Click the `BLAST` button

![blast](./imgs/blast.png)

![blast parameters](./imgs/blast_parameters.png)

### Get the results

A graphical interface is displayed with a summary of the BLAST query (queried databases, used parameters)
and the list of the hits for each query sequence.

You can see the alignment for each hit, and you can download the hits in different formats (FASTA, tabular, XML)

![blast hits](./imgs/blast_hits.png)

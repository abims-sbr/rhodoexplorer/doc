#!/usr/bin/env bash

echo "Gitlab pages for branch $CI_COMMIT_REF_NAME"

mkdir artifacts

if [ $CI_COMMIT_REF_NAME == "master" ]; then
  ARTIFACT_BRANCH="dev"
else
  ARTIFACT_BRANCH="master"
fi

echo "Get artifact https://gitlab.com/api/v4/projects/abims-sbr/rhodoexplorer/doc/jobs/artifacts/${ARTIFACT_BRANCH}/download?job=pages"

curl --location --output artifacts.zip https://gitlab.com/api/v4/projects/abims-sbr%2Frhodoexplorer%2Fdoc/jobs/artifacts/${ARTIFACT_BRANCH}/download?job=pages

unzip artifacts.zip -d artifacts

. build.sh

if [ $CI_COMMIT_REF_NAME == "master" ]; then
  mv site public
  rm -rf public/dev
  mv artifacts/public/dev public/dev
else
  mv artifacts/public public
  rm -rf public/dev
  mv site public/dev
fi
